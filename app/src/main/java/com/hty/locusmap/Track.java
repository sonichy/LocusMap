package com.hty.locusmap;

import com.baidu.mapapi.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class Track {
    String starttime = "", endtime = "", sdistance = "", sduration = "", speed="", info = "";
    List<LatLng> points = new ArrayList<>();
}