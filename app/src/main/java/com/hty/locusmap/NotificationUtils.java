package com.hty.locusmap;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

public class NotificationUtils extends ContextWrapper {

	private NotificationManager mManager;
	public static final String ANDROID_CHANNEL_ID = "com.hty.locusmap";
	public static final String ANDROID_CHANNEL_NAME = "ANDROID CHANNEL";
	Context context;

	public NotificationUtils(Context context) {
		super(context);
		this.context = context;
		if (Build.VERSION.SDK_INT >= 26) {
			createChannels();
		}
	}

	public void createChannels() {
		NotificationChannel notificationChannel = new NotificationChannel(ANDROID_CHANNEL_ID, ANDROID_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
		notificationChannel.enableLights(true);
		notificationChannel.enableVibration(true);
		notificationChannel.setLightColor(Color.GREEN);
		notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
		getManager().createNotificationChannel(notificationChannel);
	}

	private NotificationManager getManager() {
		if (mManager == null) {
			mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		}
		return mManager;
	}

	public Notification.Builder getAndroidChannelNotification(String title, String text) {
		Intent intent = new Intent(context, context.getClass());
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
		Notification.Builder builder;
		if (Build.VERSION.SDK_INT >= 26) {
			builder = new Notification.Builder(getApplicationContext(), ANDROID_CHANNEL_ID);
		} else {
			builder = new Notification.Builder(context);
		}
		builder.setContentTitle(title)
				.setContentText(text)
				.setSmallIcon(android.R.drawable.stat_notify_more)
				.setAutoCancel(true)
				.setContentIntent(pendingIntent);
		return builder;
	}

}