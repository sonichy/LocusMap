package com.hty.locusmap;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import android.util.Log;

public class Utils {

	static String sendURLResponse(String urlString) {
		Log.e("url", urlString);
		HttpURLConnection conn = null;
		URL url = null;
		String c = "";
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			conn.setRequestMethod("GET");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			c = conn.getResponseCode() + "";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			c = "?";
			e.printStackTrace();
		}
		if (conn != null) {
			conn.disconnect();
		}
		return c;
	}
}
