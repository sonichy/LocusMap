package com.hty.locusmap;

import java.util.Calendar;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MenuActivity extends Activity {
	Button button_nomap, button_map, button_history, button_server, button_set, button_about, button_quit;
	SharedPreferences sharedPreferences;
	String upload_server = "";
    private static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION };
    private static int REQUEST_PERMISSION_CODE = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MainApplication.getInstance().addActivity(this);
		setContentView(R.layout.activity_menu);
		TextView textView_version = (TextView) findViewById(R.id.textView_version);
		textView_version.setText(com.baidu.mapapi.VersionInfo.getKitName());
		button_nomap = (Button) findViewById(R.id.button_nomap);
		button_map = (Button) findViewById(R.id.button_map);
		button_history = (Button) findViewById(R.id.button_history);
		button_server = (Button) findViewById(R.id.button_server);
		button_set = (Button) findViewById(R.id.button_set);
		button_about = (Button) findViewById(R.id.button_about);
		button_quit = (Button) findViewById(R.id.button_quit);
		button_nomap.setOnClickListener(new MyOnClickListener());
		button_map.setOnClickListener(new MyOnClickListener());
		button_history.setOnClickListener(new MyOnClickListener());
		button_server.setOnClickListener(new MyOnClickListener());
		button_set.setOnClickListener(new MyOnClickListener());
		button_about.setOnClickListener(new MyOnClickListener());
		button_quit.setOnClickListener(new MyOnClickListener());
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Calendar date = Calendar.getInstance();
		TextView textView_copyright = (TextView) findViewById(R.id.textView_copyright);
		textView_copyright.setText("© Copyright 2014-" + date.get(Calendar.YEAR) + " 海天鹰版权所有");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                requestPermissions(PERMISSIONS, REQUEST_PERMISSION_CODE);
            }
        }
	}

	class MyOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.button_nomap:
				startActivity(new Intent(MenuActivity.this, NoMapMode.class));
				break;
			case R.id.button_map:
				startActivity(new Intent(MenuActivity.this, CurrentLocus.class));
				break;
			case R.id.button_history:
				startActivity(new Intent(MenuActivity.this, GPXList.class));
				break;
			case R.id.button_server:
				Intent intent = new Intent();
				upload_server = sharedPreferences.getString("uploadServer", "http://sonichy.gearhostpreview.com/locusmap");
				intent.setData(Uri.parse(upload_server));
				intent.setAction(Intent.ACTION_VIEW);
				MenuActivity.this.startActivity(intent);
				break;
			case R.id.button_set:
				startActivity(new Intent(MenuActivity.this, Setting.class));
				break;
			case R.id.button_about:
				new AlertDialog.Builder(MenuActivity.this)
						.setIcon(R.drawable.ic_launcher)
						.setTitle("轨迹地图百度版 V7.0")
						.setMessage("利用百度地图API提供的地图、定位、绘图和手机的GPS功能绘制、记录位移轨迹，查看记录的轨迹，上传GPS数据到服务器。\n作者：海天鹰\nE-mail：sonichy@163.com\nQQ：84429027\n\n更新历史\n7.0 (2022-06-09)\n1.历史轨迹增加照片所在点。\n2.读取轨迹使用类保存数据，方便复用。\n\n6.8 (2022-03-18)\n1.轨迹记录GCJ坐标转WGS，解决轨迹导入到其他地图偏移问题。\n2.文件名结尾GCJ（原来为BD），按GCJ坐标转换。\n3.修复轨迹列表跳转轨迹返回列表位置丢失问题，修复轨迹列表删除轨迹位置丢失问题。\n4.轨迹列表增加刷新菜单，轨迹长按增加分享菜单。\n\n6.7 (2022-01-12)\n1.修复历史轨迹页面改名返回列表没更新问题。\n\n6.6 (2021-11-14)\n1.轨迹列表增加搜索功能。\n\n6.5 (2021-09-03)\n1.轨迹列表和历史轨迹增加重命名菜单。\n\n6.4 (2021-03-31)\n1.增加动态权限请求，适配Android6以上。\n2.显示标题栏，方便点击菜单。\n3.支持从QQ和微信打开gpx轨迹。\n4.解决安卓10锁屏GPS休眠问题。\n5.修复轨迹偏移问题。\n6.增加分享轨迹。\n7.增加有图、无图单选逻辑。\n8.没有统计数据时，智能计算统计数据\n9.GPX增加换行和缩进。\n\n6.3 (2021-01-16)\n1.优化轨迹列表，增加图标、时间、文件大小，按时间排列。\n\n6.2 (2020-10-20)\n1.更改百度地图开放平台应用SHA1，修复无地图问题。\n2.targetSdkVersion改为24，解决MIUI全面屏虚拟键背景无法变黑问题。\n3.菜单按钮放入垂直布局居中，解决非等宽字体按钮宽度不一样问题。\n\n6.1 (2019-06-06)\n1.历史轨迹禁用旋转、Overlooking手势。\n2.修复打开服务器行程没有载入设置的服务器。\n\n6.0 (2018-09-21)\n1.升级百度地图SDK到5.2，支持设置为GCJ02坐标，解决中国国内不同地图坐标偏移问题。\n2.轨迹数据文件名增加后缀，区分创建者。\n\n5.0 (2016-10-16)\n1.地图无法加载，升级百度地图SDK到4.1，定位SDK到7.0。\n2.增加设置页面，可以设置上传服务器路径。\n\n4.2 (2016-04-08)\n1.SAE开始收费，服务器改为网眼免费空间。\n\n4.1 (2015-07-30)\n优化数据记录精度和绘图精度\n\n4.0 (2015-07-08)\n1.增加无图模式，节约内存资源。\n2.缩小精度，增加绘图的准确性。\n3.把上传路程改为上传位移，路程由位移之和来计算。\n\n3.20 (2015-06-20)\n服务器更改为新浪SAE。\n\n3.19 (2015-05-18)\njabry服务器挂了，更换成由pdxx.net提供。\n\n3.18 (2015-04-16)\n1.修复没有GPS定位而是网络定位时不绘制轨迹的问题。\n2.优化GPS开关\n\n3.17 (2015-03-07)\n1.修复轨迹文件列表删除正在记录的文件引起的崩溃\n2.增加<更新历史>菜单\n\n3.X (2014)\n增加数据记录和读取功能\n\n2.X (2014)\n开发服务器端接收上传定位数据并查看\n\n1.X (2014)\n在地图上定位当前位置")
						.setPositiveButton("确定", null).show();
				break;
			case R.id.button_quit:
				MainApplication.getInstance().exit();
				break;
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "退出");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		switch (item_id) {
		case 0:
            Log.e(Thread.currentThread().getStackTrace()[2] + "", "MainApplication.exit()");
			MainApplication.getInstance().exit();
			break;
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		String mode = MainApplication.mode;
		Drawable drawable = getResources().getDrawable(R.drawable.record);
		drawable.setBounds(0, 0, 80, 80);// 这一步必须要做,否则不会显示.
		if (mode.equals("")) {
			button_nomap.setEnabled(true);
			button_nomap.setCompoundDrawables(null, null, null, null);
			button_map.setEnabled(true);
			button_map.setCompoundDrawables(null, null, null, null);
		} else if (mode.equals("nomap")) {
			button_nomap.setEnabled(true);
			button_map.setEnabled(false);
			button_nomap.setCompoundDrawables(drawable, null, null, null);// 左
			// button_nomap.setCompoundDrawables(null, null, drawable, null);
		} else if (mode.equals("map")) {
			button_nomap.setEnabled(false);
			button_map.setEnabled(true);
			button_map.setCompoundDrawables(drawable, null, null, null);
			// button_map.setCompoundDrawables(null, null, drawable, null);
		}
	}

}