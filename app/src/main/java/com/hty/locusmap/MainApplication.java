package com.hty.locusmap;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;

public class MainApplication extends Application {
	static Context mContext;
	static String wfp="", msg="", mode="";
	private final List<Activity> list_activity = new LinkedList<>();
	private static MainApplication instance;
	static String dir_gpx = Environment.getExternalStorageDirectory().getPath() + File.separator + "LocusMap" + File.separator;

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
		if (Build.VERSION.SDK_INT >= 24) {//解决分享崩溃
			StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
			StrictMode.setVmPolicy(builder.build());
		}
	}

//	public static void setmsg(String s) {
//		msg = s;
//	}
//
//	public static String getmsg() {
//		return msg;
//	}
//
//	public static void setwfp(String s) {
//		wfp = s;
//	}
//
//	public static String getwfp() {
//		return wfp;
//	}
//
//	public static void setfn(String s) {
//		fn = s;
//	}
//
//	public static String getfn() {
//		return fn;
//	}
//
//	public static Context getContext() {
//		return mContext;
//	}

	static MainApplication getInstance() {
		if (null == instance) {
			instance = new MainApplication();
		}
		return instance;
	}

	public void addActivity(Activity activity) {
		list_activity.add(activity);
	}

	public void exit() {
		for (Activity activity : list_activity) {
			if (activity != null)
				activity.finish();
		}
		System.exit(0);
	}
}
