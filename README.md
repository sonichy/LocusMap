# Android：轨迹地图（百度地图版）
基于安卓设备 GPS + 天地图地图的超轻量级轨迹地图，可以显示当前轨迹，历史轨迹(把当日照片显示在地图上)。  
![alt](preview.jpg)
![alt](photo.jpg)

## 问题
### 默认坐标
轨迹记录使用WGS坐标，文件名(不含扩展名)以GCJ结尾的使用GCJ坐标，否则使用WGS坐标。

## 参考
[获取照片的经纬度显示在地图上](https://gitee.com/sonichy/PhotoMap)
